import java.util.HashMap;
import java.util.HashSet;
import java.util.ArrayList;
import java.util.Scanner;
import java.sql.*;
import java.text.SimpleDateFormat;

public class ClientApplication {
	final static Scanner input = new Scanner(System.in);
	static HashMap<String, Command> commands = new HashMap<String, Command>();
	static Connection c;
	static String User;
	static SimpleDateFormat format = new SimpleDateFormat("yyyy/MM/dd hh:mm:ss");
	static HashMap<String, String> columnMapper = new HashMap<String, String>();

	public static void main(String[] args) {
		boolean exitLoop = false;
		if (authDatabase()) {
			switchUser();
		} else {
			exitLoop = true;
		}

		columnMapper.put("title", "Title");
		columnMapper.put("originalTitle", "Original Title");
		columnMapper.put("genre", "Genre(s)");
		columnMapper.put("year", "Year");
		columnMapper.put("releaseDate", "Release Date");
		columnMapper.put("duration", "Duration (minutes)");
		columnMapper.put("description", "Description");
		columnMapper.put("averageRating", "Average Rating");
		columnMapper.put("totalRating", "Total Ratings");
		columnMapper.put("metascore", "Metascore");
		columnMapper.put("incomeDomestic", "Domestic Income");
		columnMapper.put("incomeWorldwide", "Worldwide Income");
		columnMapper.put("budget", "Budget");
		columnMapper.put("productionCompany", "Production Company(ies)");
		columnMapper.put("language", "Language(s)");
		columnMapper.put("userID", "User ID");
		columnMapper.put("comment", "Comment");
		columnMapper.put("name", "Name");
		columnMapper.put("birthName", "Birth Name");
		columnMapper.put("height", "Height");
		columnMapper.put("bio", "Biography");
		columnMapper.put("birthDetails", "Birth Details");
		columnMapper.put("dateOfBirth", "Date of Birth");
		columnMapper.put("dateOfDeath", "Date of Death");
		columnMapper.put("placeOfDeath", "Place of Death");
		columnMapper.put("reasonOfDeath", "Reason of Death");
		columnMapper.put("role", "Role");

        String command = "";
        while (!exitLoop) {
			System.out.println("Type \"se <movie/genre/person> <keywords>\" to search movies by title, genre, or person. Type \"hp\" to see other commands. Type \"qt\" to quit.");
			command = input.nextLine();
			if (command.length() < 2 || !commands.containsKey(command.substring(0, 2))) {
				System.out.println("Invalid command. Type \"qt\" to quit.");
			}
        	else if (commands.containsKey(command.substring(0, 2))) {
	        	switch (command.substring(0, 2)) {
	        	case "se":
	        		if (command.length() < 4) {
	        			System.out.println("Usage: " + commands.get("se").getUsage());
		        		break;
	        		}
	        		String params = command.substring(3);
	        		if (params.length() < 7) {
	        			System.out.println("Usage: " + commands.get("se").getUsage());
		        		break;
	        		} 
	        		int splitIndex = params.indexOf(" ");
	        		String type = params.substring(0, splitIndex);
	        		boolean invalid = false;
	        		if (!type.equals("movie") && !type.equals("genre") && !type.equals("person")) {
	        			invalid = true;
	        		}
	        		if ((type.equals("movie") || type.equals("genre")) && params.length() < 7) {
	        			invalid = true;
	        		}
	        		if (type.equals("person") && params.length() < 8) {
	        			invalid = true;
	        		}
	        		if (invalid) {
	        			System.out.println("Usage: " + commands.get("se").getUsage());
		        		break;
	        		}
	        		String keywords = params.substring(splitIndex+1);
	        		System.out.println("Search for " + type + " \"" + keywords + "\"");
	        		search(type, keywords);
	        		break;
				case "ar":
					if (command.length() < 4) {
						System.out.println("Usage: " + commands.get("ar").getUsage());
		        		break;
					}
					System.out.println("Add review to \"" + command.substring(3) + "\"");
					addReview(command.substring(3));
					break;
				case "er":
					if (command.length() < 4) {
						System.out.println("Usage: " + commands.get("er").getUsage());
		        		break;
					}
					System.out.println("Edit review on \"" + command.substring(3) + "\"");
					editReview(command.substring(3));
					break;
				case "dr":
					if (command.length() < 4) {
						System.out.println("Usage: " + commands.get("dr").getUsage());
		        		break;
					}
					System.out.println("Delete review from \"" + command.substring(3) + "\"");
					deleteReview(command.substring(3));
					break;
				case "sw":
					System.out.println("Switching user");
					switchUser();
					break;
				case "hp":
					System.out.println("Commands menu");
					System.out.println("Name / Command / Usage / Description");
					for (HashMap.Entry<String, Command> iter : commands.entrySet()) {
						System.out.println(iter.getValue().getName() + " / " + iter.getKey() + " / " + iter.getValue().getUsage() + " / " + iter.getValue().getDescription());
					}
					System.out.println("-------------------------------------------------");
					break;
				case "qt":
					System.out.println("Program exit.");
					input.close();
					System.exit(0);
				case "am":
					System.out.println("Add movie to database");
					addMovie();
					break;
				case "em":
					if (command.length() < 4) {
						System.out.println("Usage: " + commands.get("em").getUsage());
		        		break;
					}
					System.out.println("Edit movie \"" + command.substring(3) + "\"");
					editMovie(command.substring(3));
					break;
				case "dm":
					if (command.length() < 4) {
						System.out.println("Usage: " + commands.get("dm").getUsage());
		        		break;
					}
					System.out.println("Delete movie \"" + command.substring(3) + "\"");
					deleteMovie(command.substring(3));
					break;
				case "ap":
					System.out.println("Add person to database");
					addPerson();
					break;
				case "ep":
					if (command.length() < 4) {
						System.out.println("Usage: " + commands.get("ep").getUsage());
		        		break;
					}
					System.out.println("Edit person \"" + command.substring(3) + "\"");
					editPerson(command.substring(3));
					break;
				case "dp":
					if (command.length() < 4) {
						System.out.println("Usage: " + commands.get("dp").getUsage());
		        		break;
					}
					System.out.println("Delete person \"" + command.substring(3) + "\"");
					deletePerson(command.substring(3));
					break;
				case "af":
					if (command.length() < 4) {
						System.out.println("Usage: " + commands.get("af").getUsage());
		        		break;
					}
					System.out.println("Add to your favourites \"" + command.substring(3) + "\"");
					addFavourite(command.substring(3));
					break;
				case "df":
					if (command.length() < 4) {
						System.out.println("Usage: " + commands.get("df").getUsage());
		        		break;
					}
					System.out.println("Delete from your favourites \"" + command.substring(3) + "\"");
					deleteFavourite(command.substring(3));
					break;
				default:
					System.out.println("Invalid command. Type \"qt\" to quit.");
					break;
				}
			}
		}
		System.out.println("Program exit.");
		input.close();
		System.exit(0);
	}

	public static int printQueryResults(ResultSet rs, HashSet<String> attributes) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		int setItem = 0;
		int colIter = 1;
		HashSet<Integer> matchingColumns = new HashSet<Integer>();
	    
		for (int i = 1; i <= columnsNumber; i++) {
			if (attributes.contains(rsmd.getColumnName(i))) {
				matchingColumns.add(i);
				if (columnMapper.containsKey(rsmd.getColumnName(i))) {
	        		System.out.print(columnMapper.get(rsmd.getColumnName(i)) + ", ");
				}
			}
	    }
	    System.out.println();
		while (rs.next()) {
			setItem++;
			System.out.print(setItem + ". ");
		    for (int i = 1; i <= columnsNumber; i++) {
				if (matchingColumns.contains(i)) {
					if (colIter > 1) System.out.print(",  ");
					String columnValue = rs.getString(i);
					System.out.print(columnValue);
					colIter++;
				}
		    }
			colIter = 1;
		    System.out.println();
		}
		return setItem;
	}

	public static int printQueryResults(ResultSet rs, int row) throws SQLException {
		ResultSetMetaData rsmd = rs.getMetaData();
		int columnsNumber = rsmd.getColumnCount();
		int setItem = 0;
	    System.out.println();
		while (rs.next()) {
			setItem++;
			if (setItem == row) {
				for (int i = 2; i <= columnsNumber; i++) {
					if (columnMapper.containsKey(rsmd.getColumnName(i))) {
						System.out.print(columnMapper.get(rsmd.getColumnName(i)) + ": ");
					}
					String columnValue = rs.getString(i);
					System.out.print(columnValue);
					System.out.println();
				}
				break;
			}
		}
		System.out.println();
		return setItem;
	}

	public static void search(String type, String keywords) {
		String searchQueryString = "";
		String sort = "";
		boolean exitLoop = false;
		String sortString = "";
		
		
		try {
			// PreparedStatement query = null;
			ResultSet rs = null;
			Statement st = c.createStatement(ResultSet.TYPE_SCROLL_INSENSITIVE, ResultSet.CONCUR_READ_ONLY);
			String query = "";
			String searchMovieQueryString = "select "
				+ "movieID, "
				+ "title, "
				+ "originalTitle, "
				+ "group_concat(genre separator ', ') AS genre, "
				+ "year, "
				+ "substring_index(releaseDate, ' ', 1) as releaseDate, "
				+ "duration, "
				+ "description, "
				+ "averageRating, "
				+ "totalRating, "
				+ "metascore, "
				+ "incomeDomestic, "
				+ "incomeWorldwide, "
				+ "budget, "
				+ "productionCompany, "
				+ "language "
				+ "from "
				+ "Movies left outer join Income using (movieID) "
				+ "left outer join Budget using (movieID) "
				+ "left outer join ProductionCompaniesInMovies using (movieID) "
				+ "left outer join LanguagesInMovies using (movieID) "
				+ "left outer join GenresInMovies using (movieID) "
				+ "where ";

			String searchPersonQueryString = "select personID, name, "
			+ "birthName, "
			+ "height, "
			+ "bio, "
			+ "birthDetails, "
			+ "substring_index(dateOfBirth, ' ', 1) as dateOfBirth, "
			+ "substring_index(dateOfDeath, ' ', 1) as dateOfDeath, "
			+ "placeOfDeath, "
			+ "reasonOfDeath "
			+ "from People "
			+ "left outer join DeadPeople using (personID) "
			+ "left outer join PeopleInMovies using (personID) "
			+ "left outer join Movies using (movieID) "
			+ "where personID = '%s'";

			HashSet<String> basicAttributes = new HashSet<String>();
			switch (type) {
			case "movie":
				while (!exitLoop) {
					System.out.println("Type \"t\" to sort by title. Type \"p\" to sort by popularity. Press enter to sort default.");
					sort = input.nextLine();
					if (!sort.equals("t") && !sort.equals("p")) {
						if (sort.equals("")) {
							exitLoop = true;
						} else {
							System.out.println("Invalid command.");
						}
					} else {
						exitLoop = true;
					}
				}
				
				switch (sort) {
					case "t":
						sortString = "order by title";
						break;
					case "p":
						sortString = "order by totalRating desc";
						break;
					default: break;
				}


				basicAttributes.add("title");
				basicAttributes.add("genre");
				basicAttributes.add("releaseDate");
				basicAttributes.add("averageRating");
				basicAttributes.add("totalRating");

				searchQueryString = searchMovieQueryString + "title like '%%%s%%' group by movieID %s limit 10";
				keywords = keywords.replace("\'", "\\'");
				query = String.format(searchQueryString, keywords, sortString);
				rs = st.executeQuery(query);
				int numResults = printQueryResults(rs, basicAttributes);

				boolean exitLoop2 = false;
				System.out.println();
				System.out.println("Found " + numResults + " result(s). To show advanced details for a movie, type the corresponding item number.");
				System.out.println("Type \'0\' to exit.");
				while (!exitLoop2) {
					String command = input.nextLine();
					if (Integer.parseInt(command) < 0 || Integer.parseInt(command) > numResults) {
						System.out.println("Index out of range.");
					}
					else if (Integer.parseInt(command) == 0) {
						exitLoop2 = true;
					}
					else {
						rs.beforeFirst();
						int setItem = 0;
						printQueryResults(rs, Integer.parseInt(command));
						rs.beforeFirst();
						while (rs.next()) {
							setItem++;
							if (setItem == Integer.parseInt(command)) {
								String movieID = rs.getString(1);
								searchQueryString = String.format("select * from Reviews where movieID = '%s'", movieID);
								ResultSet rs2 = st.executeQuery(searchQueryString);
								System.out.println("User reviews for the movie");
								HashSet<String> userReviewSet = new HashSet<String>();
								userReviewSet.add("userID");
								userReviewSet.add("comment");
								printQueryResults(rs2, userReviewSet);
								
								break;
							}
						}

						exitLoop2 = true;
					}
				}
				basicAttributes.clear();
				break;
			case "genre":
				while (!exitLoop) {
					System.out.println("Type \"t\" to sort by title. Type \"p\" to sort by popularity. Press enter to sort default.");
					sort = input.nextLine();
					if (!sort.equals("t") && !sort.equals("p")) {
						if (sort.equals("")) {
							exitLoop = true;
						} else {
							System.out.println("Invalid command.");
						}
					} else {
						exitLoop = true;
					}
				}
				System.out.println();
				
				switch (sort) {
					case "t":
						sortString = "order by title";
						break;
					case "p":
						sortString = "order by totalRating desc";
						break;
					default: break;
				}

				basicAttributes.add("title");
				basicAttributes.add("genre");
				basicAttributes.add("releaseDate");
				basicAttributes.add("averageRating");
				basicAttributes.add("totalRating");

				searchQueryString = "select "
					+ "movieID, "
					+ "title, "
					+ "group_concat(genre separator ', ') AS genre, "
					+ "substring_index(releaseDate, ' ', 1) as releaseDate, "
					+ "description, "
					+ "totalRating "
					+ "from Movies left outer join GenresInMovies using (movieID) where genre like '%%%s%%' group by movieID %s limit 10";
				
				query = String.format(searchQueryString, keywords, sortString);
				rs = st.executeQuery(query);
				numResults = printQueryResults(rs, basicAttributes);

				exitLoop2 = false;
				System.out.println();
				System.out.println("Found " + numResults + " result(s). To show advanced details for a movie, type the corresponding item number.");
				System.out.println("Type \'0\' to exit.");
				while (!exitLoop2) {
					String command = input.nextLine();
					if (Integer.parseInt(command) < 0 || Integer.parseInt(command) > numResults) {
						System.out.println("Index out of range.");
					}
					else if (Integer.parseInt(command) == 0) {
						exitLoop2 = true;
					}
					else {
						rs.beforeFirst();
						int setItem = 0;
						while (rs.next()) {
							setItem++;
							if (setItem == Integer.parseInt(command)) {
								String movieID = rs.getString(1);
								searchQueryString = searchMovieQueryString + "movieID = '" + movieID + "'";
								ResultSet rs2 = st.executeQuery(searchQueryString);
								printQueryResults(rs2, 1);
								break;
							}
						}
						exitLoop2 = true;
					}
				}

				basicAttributes.clear();

				break;
			case "person":
				while (!exitLoop) {
					System.out.println("Type \"n\" to sort by name. Type \"d\" to sort by date of birth. Press enter to sort default.");
					sort = input.nextLine();
					if (!sort.equals("n") && !sort.equals("d")) {
						if (sort.equals("")) {
							exitLoop = true;
						} else {
							System.out.println("Invalid command.");
						}
					} else {
						exitLoop = true;
					}
				}
				
				switch (sort) {
					case "n":
						sortString = "order by name";
						break;
					case "d":
						sortString = "order by dateOfBirth desc";
						break;
					default: break;
				}

				basicAttributes.add("name");
				basicAttributes.add("birthName");
				basicAttributes.add("height");
				basicAttributes.add("birthDetails");
				basicAttributes.add("dateOfBirth");

				searchQueryString = "select personID, name, birthName, height, bio, birthDetails, substring_index(dateOfBirth, ' ', 1) as dateOfBirth, substring_index(dateOfDeath, ' ', 1) as dateOfDeath, placeOfDeath, reasonOfDeath from People left outer join DeadPeople using (personID) left outer join PeopleInMovies using (personID) left outer join Movies using (movieID) where name like '%%%s%%' group by personID %s limit 10";
				query = String.format(searchQueryString, keywords, sortString);
				rs = st.executeQuery(query);
				numResults = printQueryResults(rs, basicAttributes);

				exitLoop2 = false;
				System.out.println();
				System.out.println("Found " + numResults + " result(s). To show advanced details for a person, type the corresponding item number.");
				System.out.println("Type \'0\' to exit.");
				while (!exitLoop2) {
					String command = input.nextLine();
					if (Integer.parseInt(command) < 0 || Integer.parseInt(command) > numResults) {
						System.out.println("Index out of range.");
					}
					else if (Integer.parseInt(command) == 0) {
						exitLoop2 = true;
					}
					else {
						rs.beforeFirst();
						int setItem = 0;
						while (rs.next()) {
							setItem++;
							if (setItem == Integer.parseInt(command)) {
								String personID = rs.getString(1);
								// searchQueryString = searchPersonQueryString;
								searchQueryString = String.format(searchPersonQueryString, personID);
								ResultSet rs2 = st.executeQuery(searchQueryString);
								printQueryResults(rs2, 1);

								searchQueryString = String.format("select * from PeopleInMovies left outer join movies using (movieID) where personID = '%s'", personID);
								ResultSet rs3 = st.executeQuery(searchQueryString);

								System.out.println("Related movies");
								HashSet<String> peopleInMoviesSet = new HashSet<String>();
								peopleInMoviesSet.add("role");
								peopleInMoviesSet.add("title");
								peopleInMoviesSet.add("year");

								printQueryResults(rs3, peopleInMoviesSet);
								break;
							}
						}
						exitLoop2 = true;
					}
				}

				basicAttributes.clear();

				break;
			default: break;
			}
		} catch (Exception e) {
			System.out.println(e);
		}
		System.out.println();
	}

	public static void addFavourite(String entity) {
		String searchMovieQueryString = "select * from Movies where title like '%s%%'";
		try {
			Statement st = c.createStatement();
			String query = String.format(searchMovieQueryString, entity);
			ResultSet rs = st.executeQuery(query);

			if (rs.next()) {
				String movieID = rs.getString(1);
				query = String.format("select * from UsersFavouriteMovies where movieID = '%s' and userID = '%s'", movieID, User);
				rs = st.executeQuery(query);
				if (rs.next()) {
					System.out.println("You have already added this movie to your favourites.");
				} else {

					query = String.format("insert into UsersFavouriteMovies values ('%s', '%s')", User, movieID);
//					System.out.println(query);
					st.execute(query);

					System.out.println("Movie successfully added to favourites.");
				}
			} else {
				System.out.println("Movie not found.");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}
	
	public static void deleteFavourite(String entity) {
		String searchMovieQueryString = "select * from Movies where title like '%s%%'";
		
		try {
			Statement st = c.createStatement();
			String query = String.format(searchMovieQueryString, entity);
			ResultSet rs = st.executeQuery(query);

			if (rs.next()) {
				String movieID = rs.getString(1);
				System.out.println("Are you sure you want to delete this movie from your favourites?");
				System.out.println("Type y to confirm. Type any other character to cancel.");
				String command = "";
				command = input.nextLine();
				if (command.equals("y")) {

					query = String.format("select * from UsersFavouriteMovies where movieID = '%s' and userID = '%s'", movieID, User);
					rs = st.executeQuery(query);
					if (!rs.next()) {
						System.out.println("This movie is not in your favourites.");
					} else {
						query = String.format("delete from UsersFavouriteMovies where movieID = '%s' and userID = '%s'", movieID, User);
//						System.out.println(query);
						st.execute(query);

						System.out.println("Movie successfully deleted from favourites.");
					}

				} else {
					System.out.println("Action cancelled.");
				}
			} else {
				System.out.println("Movie not found.");
			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}
	
	public static void addReview(String entity) {
		String searchMovieQueryString = "select * from Movies where title like '%s%%'";
		try {
			Statement st = c.createStatement();
			String query = String.format(searchMovieQueryString, entity);
			ResultSet rs = st.executeQuery(query);

			if (rs.next()) {
				String movieID = rs.getString(1);
				String movieTitle = rs.getString(2);
				query = String.format("select * from Reviews where movieID = '%s' and userID = '%s'", movieID, User);
				rs = st.executeQuery(query);
				if (rs.next()) {
					System.out.println("You have already added a review for " + movieTitle + ".");
				} else {

					// Give user option to add review to movie
					// insert into <table-name> values ...

					System.out.println("Write your review for " + movieTitle + ".");
					String reviewContent = input.nextLine();
					query = String.format("insert into Reviews values ('%s', '%s', '%s')", movieID, User, reviewContent);
//					System.out.println(query);
					st.execute(query);

					System.out.println("Review added successfully.");
				}
			} else {
				System.out.println("Movie not found.");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void editReview(String entity) {
		String searchMovieQueryString = "select * from Movies where title like '%s%%'";
		try {
			Statement st = c.createStatement();
			String query = String.format(searchMovieQueryString, entity);
			ResultSet rs = st.executeQuery(query);

			if (rs.next()) {
				String movieID = rs.getString(1);
				String movieTitle = rs.getString(2);
				query = String.format("select * from Reviews where movieID = '%s' and userID = '%s'", movieID, User);
				rs = st.executeQuery(query);
				if (!rs.next()) {
					System.out.println("You do not have a review for this movie to edit.");
				} else {
					// Let user edit their review (there should only be one)
					// update <table-reference> set ... where ...

					System.out.println("Write your new review for " + movieTitle + ".");
					String reviewContent = input.nextLine();
					query = String.format("update Reviews set comment = '%s' where movieID = '%s' and userID = '%s'", reviewContent, movieID, User);
//					System.out.println(query);
					st.execute(query);

					System.out.println("Review edited successfully.");
				}
			} else {
				System.out.println("Movie not found.");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void deleteReview(String entity) {
		String searchMovieQueryString = "select * from Movies where title like '%s%%'";
		
		try {
			Statement st = c.createStatement();
			String query = String.format(searchMovieQueryString, entity);
			ResultSet rs = st.executeQuery(query);

			if (rs.next()) {
				String movieID = rs.getString(1);
				String movieTitle = rs.getString(2);
				System.out.println("Are you sure you want to delete your review for " + movieTitle + "?");
				System.out.println("Type y to confirm. Type any other character to cancel.");
				String command = "";
				command = input.nextLine();
				if (command.equals("y")) {

					query = String.format("select * from Reviews where movieID = '%s' and userID = '%s'", movieID, User);
					rs = st.executeQuery(query);
					if (!rs.next()) {
						System.out.println("You do not have a review for this movie.");
					} else {
						query = String.format("delete from Reviews where movieID = '%s' and userID = '%s'", movieID, User);
//						System.out.println(query);
						st.execute(query);

						System.out.println("Review successfully deleted.");
					}

				} else {
					System.out.println("Action cancelled.");
				}
			} else {
				System.out.println("Movie not found.");
			}

		} catch (Exception e) {
			System.out.println(e);
		}

	}

	public static void switchUser() {
		String command = "";
		//Available commands for admins and users include search, add review, edit review, delete review, help, switch, quit
		//Available commands for admins include add movie, edit movie, delete movie
		Command searchCommand = new Command("Search", "se", "se <movie/genre/person> <keywords>", "Search for movies by title, genre, or person.");
		Command addReviewCommand = new Command("Add Review", "ar", "ar <title>", "Add a review to the selected movie.");
		Command editReviewCommand = new Command("Edit Review", "er", "er <title>", "Edit the review on the selected movie.");
		Command deleteReviewCommand = new Command("Delete Review", "dr", "dr <title>", "Delete the review from the selected movie.");
		Command switchCommand = new Command("Switch", "sw", "sw", "Switch between an admin and user role.");
		Command helpCommand = new Command("Help", "hp", "hp", "View the list of currently available commands.");
		Command quitCommand = new Command("Quit", "qt", "qt", "Exits the program.");
		Command addMovieCommand = new Command("Add Movie", "am", "am", "Add a movie to the database.");
		Command editMovieCommand = new Command("Edit Movie", "em", "em <title>", "Edit the selected movie in the database.");
		Command deleteMovieCommand = new Command("Delete Movie", "dm", "dm <title>", "Delete the selected movie from the database.");
		Command addPersonCommand = new Command("Add Person", "ap", "ap", "Add a person to the database.");
		Command editPersonCommand = new Command("Edit Person", "ep", "ep <name>", "Edit the selected person in the database.");
		Command deletePersonCommand = new Command("Delete Person", "dp", "dp <name>", "Delete the selected person from the database.");
		Command addFavouriteCommand = new Command("Add Favourite Movie", "af", "af <title>", "Add the selected movie to favourites.");
		Command deleteFavouriteCommand = new Command("Delete Favourite Movie", "df", "df <title>", "Delete the selected movie from favourites.");


		// HashMap<String, Command> commands = new HashMap<String, Command>();

		HashMap<String, Command> defaultCommands = new HashMap<String, Command>();
		defaultCommands.put(searchCommand.getCmd(), searchCommand);
		defaultCommands.put(addReviewCommand.getCmd(), addReviewCommand);
		defaultCommands.put(editReviewCommand.getCmd(), editReviewCommand);
		defaultCommands.put(deleteReviewCommand.getCmd(), deleteReviewCommand);
		defaultCommands.put(addFavouriteCommand.getCmd(), addFavouriteCommand);
		defaultCommands.put(deleteFavouriteCommand.getCmd(), deleteFavouriteCommand);
		defaultCommands.put(switchCommand.getCmd(), switchCommand);
		defaultCommands.put(helpCommand.getCmd(), helpCommand);
		defaultCommands.put(quitCommand.getCmd(), quitCommand);

		HashMap<String, Command> adminCommands = new HashMap<String, Command>();
		adminCommands.put(addMovieCommand.getCmd(), addMovieCommand);
		adminCommands.put(editMovieCommand.getCmd(), editMovieCommand);
		adminCommands.put(deleteMovieCommand.getCmd(), deleteMovieCommand);
		adminCommands.put(addPersonCommand.getCmd(), addPersonCommand);
		adminCommands.put(editPersonCommand.getCmd(), editPersonCommand);
		adminCommands.put(deletePersonCommand.getCmd(), deletePersonCommand);

		boolean exitLoop = false;
		while (!exitLoop) {
			System.out.println("Type \"a\" to use program as an admin. Type \"u\" to use program as a user. Type \"qt\" to quit.");
			command = input.nextLine();
			if (command != "a" && command != "u" && command != "qt") {
				exitLoop = true;
			}
			else {
				System.out.println("Invalid command.");
			}
		}

		if (command.equals("a")) {
			commands.clear();
			commands.putAll(adminCommands);
			commands.putAll(defaultCommands);
			System.out.println("Entering program as an admin.");
		}
		else if (command.equals("u")) {
			commands.clear();
			commands.putAll(defaultCommands);
			System.out.println("Entering program as a user.");
		}
		else {
			System.out.println("Program exit.");
			System.exit(0);
		}
	}

	public static boolean authDatabase() {
		String server, pwd, db;
		System.out.print("Enter the SQL server to connect to: ");
		server = input.nextLine();
		// server = "127.0.0.1:3306";
		// server = "marmoset03.shoshin.uwaterloo.ca";
		System.out.print("Enter username for the SQL server: ");
		User = input.nextLine();
		pwd = new String(System.console().readPassword("Enter password for the SQL server: "));
		System.out.print("Enter database for the SQL server: ");
		try {
			Class.forName("com.mysql.cj.jdbc.Driver");
		} catch (Exception e) {
			System.err.print(e);
		}
		db = input.nextLine();
		try {
			c = DriverManager.getConnection("jdbc:mysql://" + server, User, pwd);
			Statement st = c.createStatement();
			String query = "use " + db;
			st.execute(query);
			// add user to Users table if not exists
			query = "select * from Users where userID = '" + User + "'";
			ResultSet rs = st.executeQuery(query);
			if (!rs.next()) {
				query = "insert into Users values ('" + User + "')";
				st.execute(query);
			}
		} catch (Exception e) {
			System.out.println(e);
			return false;
		}
		return true;
	}

	public static void addMovie() {
		// User fills in everything and confirms values]
		try {
			ArrayList<String> genres = new ArrayList<>();
			ArrayList<String> languages = new ArrayList<>();
			ArrayList<String> countries = new ArrayList<>();
			ArrayList<String> productionCompanies = new ArrayList<>();
			HashMap<String, String> people = new HashMap<String, String>();
			HashMap<String, String> values = new HashMap<String, String>();
			boolean exitLoop = false;
			while (!exitLoop) {
				System.out.println("Use the following form to add a movie entry");
				System.out.println("Type the number at the beginning of the line to edit the property.");
				System.out.println("Required fields are marked with a '*'.");
				System.out.println("Type \'c\' to confirm changes. Type \'x\' to cancel.");
				System.out.println("1: Title *");
				System.out.println("2: Description");
				System.out.println("3: Release date");
				System.out.println("4: Genre(s)");
				System.out.println("5: Cast member(s)");
				System.out.println("6: Duration");
				System.out.println("7: Language(s)");
				System.out.println("8: Country(ies)");
				System.out.println("9: Production company(ies)");
				System.out.println("10: Budget");
				System.out.println("11: Domestic Income");
				System.out.println("12: Worldwide Income");
				String command = input.nextLine().trim();
				switch(command) {
				case "1":
					System.out.println("Enter a title");
					command = input.nextLine().trim();
					if (command.equals("")) {
						System.out.println("This field is required.");
					} else {
						values.put("title", command);
					}
					break;
				case "2":
					System.out.println("Enter a description");
					command = input.nextLine().trim();
					values.put("description", command);
					break;
				case "3":
					System.out.println("Enter release date (yyyy/mm/dd)");
					command = input.nextLine().trim();
					values.put("releaseDate", (new java.sql.Timestamp(format.parse(command + " 00:00:00").getTime())).toString());
					break;
				case "4":
					System.out.println("Search for a genre");
					genres.add(searchCriteria("Genres", "genre"));
					break;
				case "5":
					System.out.println("Search for a cast member");
					command = input.nextLine().trim();
					Statement st = c.createStatement();
					String query = "select personID, firstName from People where firstName like '" + command + "%' limit 10";
					ResultSet rs = st.executeQuery(query);
					String results[] = new String[10];
					System.out.println("Select an option");
					int i = 1;
					while (rs.next()) {
						String columnValue = rs.getString(1);
						results[i - 1] = columnValue;
						System.out.println(i + ": " + rs.getString(2));
						i++;
					}
					command = input.nextLine().trim();
					System.out.println("Enter their role");
					String role = input.nextLine().trim();
					people.put(results[Integer.parseInt(command) - 1], role);
					break;
				case "6":
					System.out.println("Enter a duration (min)");
					command = input.nextLine().trim();
					values.put("duration", command);
					break;
				case "7":
					System.out.println("Search for a language");
					languages.add(searchCriteria("Languages", "language"));
					break;
				case "8":
					System.out.println("Search for a country");
					countries.add(searchCriteria("Countries", "country"));
					break;
				case "9":
					System.out.println("Enter a production company");
					productionCompanies.add(searchCriteria("ProductionCompany", "productionCompany"));
					break;
				case "10":
					System.out.println("Enter a budget");
					command = input.nextLine().trim();
					values.put("budget", command);
					break;
				case "11":
					System.out.println("Enter domestic income");
					command = input.nextLine().trim();
					values.put("incomeDomestic", command);
					break;
				case "12":
					System.out.println("Enter worldwide income");
					command = input.nextLine().trim();
					values.put("incomeWorldwide", command);
					break;
				case "c":
					exitLoop = true;
					st = c.createStatement();
					rs = st.executeQuery("select entityNum from MaxIDs where entityName = 'movie'");
					int maxId = 0;
					while (rs.next()) {
						maxId = Integer.parseInt(rs.getString(1));
					}
					maxId++;

					query = String.format("insert into Movies (movieID, title, description, releaseDate, duration) " +
						"values ('%s', '%s', '%s', %s, %s)",
						"tt" + maxId,
						values.get("title"),
						values.containsKey("description") ? values.get("description") : "",
						values.containsKey("releaseDate") ? "'" + values.get("releaseDate") + "'" : "null",
						values.containsKey("duration") ? values.get("duration") : "0"
					);
					st.executeUpdate(query);

					if (!genres.isEmpty()) {
						query = "insert into GenresInMovies (movieID, genre) values ('tt" + maxId + "','";
						query += String.join("'),('tt" + maxId + "','", genres);
						query += "');";
						st.executeUpdate(query);
					}
					if (!languages.isEmpty()) {
						query = "insert into LanguagesInMovies (movieID, language) values ('tt" + maxId + "','";
						query += String.join("'),('tt" + maxId + "','", languages);
						query += "');";
						st.executeUpdate(query);
					}
					if (!countries.isEmpty()) {
						query = "insert into CountriesInMovies (movieID, country) values ('tt" + maxId + "','";
						query += String.join("'),('tt" + maxId + "','", countries);
						query += "');";
						st.executeUpdate(query);
					}
					if (!productionCompanies.isEmpty()) {
						query = "insert into ProductionCompaniesInMovies (movieID, productionCompany) values ('tt" + maxId + "','";
						query += String.join("'),('tt" + maxId + "','", productionCompanies);
						query += "');";
						st.executeUpdate(query);
					}

					if (!people.isEmpty()) {
						query = "insert into PeopleInMovies (movieID, personID, role) values ";
						for (HashMap.Entry<String, String> entry : people.entrySet()) {
							String personID = entry.getKey();
							role = entry.getValue();
							query += String.format("('%s','%s','%s'),",
								"tt" + maxId,
								personID,
								role
							);
						}
						query = query.substring(0, query.length() - 1) + ";";
						st.executeUpdate(query);
					}

					query = "update MaxIDs set entityNum = " + maxId + " where entityName = 'movie'";
					st.executeUpdate(query);

					System.out.println(String.format("Movie successfully added (tt%s).", maxId));
					break;
				case "x":
					exitLoop = true;
					break;
				default:
					System.out.println("Invalid command " + command);
					break;
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static String searchCriteria(String table, String criteria) throws Exception {
		String command = input.nextLine().trim();
		Statement st = c.createStatement();
		String query = "select " + criteria + " from " + table + " where " + criteria + " like '" + command + "%' limit 10";
		ResultSet rs = st.executeQuery(query);
		String results[] = new String[10];
		System.out.println("Select an option");
		int i = 1;
		while (rs.next()) {
			String columnValue = rs.getString(1);
			results[i - 1] = columnValue;
			System.out.println(i + ": " + columnValue);
			i++;
		}
		command = input.nextLine().trim();
		return results[Integer.parseInt(command) - 1];
	}

	public static void editMovie(String q) {
		try {
			ArrayList<String> genres = new ArrayList<>();
			ArrayList<String> languages = new ArrayList<>();
			ArrayList<String> countries = new ArrayList<>();
			ArrayList<String> productionCompanies = new ArrayList<>();
			HashMap<String, String> people = new HashMap<String, String>();
			HashMap<String, String> values = new HashMap<String, String>();
			Statement st = c.createStatement();
			String query = "select movieID, title from Movies where title like '" + q + "%' limit 10";
			ResultSet rs = st.executeQuery(query);
			String movies[] = new String[10];
			System.out.println("Select a movie to edit");
			int i = 1;
			while (rs.next()) {
				movies[i - 1] = rs.getString(1);
				System.out.println(i + ": " + rs.getString(2));
				i++;
			}
			String command = input.nextLine().trim();
			String movieID = movies[Integer.parseInt(command) - 1];
			boolean exitLoop = false;
			while (!exitLoop) {
				System.out.println("Use the following form to edit a movie entry");
				System.out.println("Type the number at the beginning of the line to edit the property.");
				System.out.println("Type \'c\' to confirm your changes. Type \'x\' to cancel.");
				System.out.println("1: Title");
				System.out.println("2: Description");
				System.out.println("3: Release date");
				System.out.println("4: Genre(s)");
				System.out.println("5: Cast member(s)");
				System.out.println("6: Duration");
				System.out.println("7: Language(s)");
				System.out.println("8: Country(ies)");
				System.out.println("9: Production company(ies)");
				System.out.println("10: Budget");
				System.out.println("11: Domestic Income");
				System.out.println("12: Worldwide Income");
				command = input.nextLine().trim();
				switch(command) {
				case "1":
					System.out.println("Enter a title");
					command = input.nextLine().trim();
					values.put("title", command);
					break;
				case "2":
					System.out.println("Enter a description");
					command = input.nextLine().trim();
					values.put("description", command);
					break;
				case "3":
					System.out.println("Enter release date (yyyy/mm/dd)");
					command = input.nextLine().trim();
					values.put("releaseDate", (new java.sql.Timestamp(format.parse(command + " 00:00:00").getTime())).toString());
					break;
				case "4":
					System.out.println("Search for a genre");
					genres.add(searchCriteria("Genres", "genre"));
					break;
				case "5":
					System.out.println("Search for a cast member");
					command = input.nextLine().trim();
					query = "select personID, firstName from People where firstName like '" + command + "%' limit 10";
					rs = st.executeQuery(query);
					String results[] = new String[10];
					System.out.println("Select an option");
					i = 1;
					while (rs.next()) {
						String columnValue = rs.getString(1);
						results[i - 1] = columnValue;
						System.out.println(i + ": " + rs.getString(2));
						i++;
					}
					command = input.nextLine().trim();
					System.out.println("Enter their role");
					String role = input.nextLine().trim();
					people.put(results[Integer.parseInt(command) - 1], role);
					break;
				case "6":
					System.out.println("Enter a duration (min)");
					command = input.nextLine().trim();
					values.put("duration", command);
					break;
				case "7":
					System.out.println("Search for a language");
					languages.add(searchCriteria("Languages", "language"));
					break;
				case "8":
					System.out.println("Search for a country");
					countries.add(searchCriteria("Countries", "country"));
					break;
				case "9":
					System.out.println("Enter a production company");
					productionCompanies.add(searchCriteria("ProductionCompany", "productionCompany"));
					break;
				case "10":
					System.out.println("Enter a budget");
					command = input.nextLine().trim();
					values.put("budget", command);
					break;
				case "11":
					System.out.println("Enter domestic income");
					command = input.nextLine().trim();
					values.put("incomeDomestic", command);
					break;
				case "12":
					System.out.println("Enter worldwide income");
					command = input.nextLine().trim();
					values.put("incomeWorldwide", command);
					break;
				case "c":
					exitLoop = true;
					for (HashMap.Entry<String, String> entry : values.entrySet()) {
						query = String.format("update Movies set %s = '%s' where movieID = '%s';", entry.getKey(), entry.getValue(), movieID);
						st.executeUpdate(query);
					}

					if (!genres.isEmpty()) {
						query = String.format("delete from GenresInMovies where movieID = '%s';", movieID);
						st.executeUpdate(query);
						query = "insert into GenresInMovies (movieID, genre) values ('" + movieID + "','";
						query += String.join("'),('" + movieID + "','", genres);
						query += "');";
						st.executeUpdate(query);
					}
					if (!languages.isEmpty()) {
						query = String.format("delete from LanguagesInMovies where movieID = '%s';", movieID);
						st.executeUpdate(query);
						query = "insert into LanguagesInMovies (movieID, language) values ('" + movieID + "','";
						query += String.join("'),('" + movieID + "','", languages);
						query += "');";
						st.executeUpdate(query);
					}
					if (!countries.isEmpty()) {
						query = String.format("delete from CountriesInMovies where movieID = '%s';", movieID);
						st.executeUpdate(query);
						query = "insert into CountriesInMovies (movieID, country) values ('" + movieID + "','";
						query += String.join("'),('" + movieID + "','", countries);
						query += "');";
						st.executeUpdate(query);
					}
					if (!productionCompanies.isEmpty()) {
						query = String.format("delete from ProductionCompaniesInMovies where movieID = '%s';", movieID);
						st.executeUpdate(query);
						query = "insert into ProductionCompaniesInMovies (movieID, productionCompany) values ('" + movieID + "','";
						query += String.join("'),('" + movieID + "','", productionCompanies);
						query += "');";
						st.executeUpdate(query);
					}

					if (!people.isEmpty()) {
						query = String.format("delete from PeopleInMovies where movieID = '%s';", movieID);
						st.executeUpdate(query);
						query = "insert into PeopleInMovies (movieID, personID, role) values ";
						for (HashMap.Entry<String, String> entry : people.entrySet()) {
							String personID = entry.getKey();
							role = entry.getValue();
							query += String.format("('%s','%s','%s'),",
								"tt" + movieID,
								personID,
								role
							);
						}
						query = query.substring(0, query.length() - 1) + ";";
						st.executeUpdate(query);
					}

					System.out.println(String.format("Movie successfully edited (%s).", movieID));
					break;
				case "x":
					exitLoop = true;
					break;
				default:
					System.out.println("Invalid command " + command);
					break;
				}
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void deleteMovie(String q) {
		try {
			Statement st = c.createStatement();
			String query = "select movieID, title from Movies where title like '" + q + "%' limit 10";
			ResultSet rs = st.executeQuery(query);
			String movies[] = new String[10];
			System.out.println("Select a movie to delete");
			int i = 1;
			while (rs.next()) {
				movies[i - 1] = rs.getString(1);
				System.out.println(i + ": " + rs.getString(2));
				i++;
			}
			String command = input.nextLine().trim();
			String movieID = movies[Integer.parseInt(command) - 1];
			System.out.println("Are you sure you want to delete this movie? Type y to confirm. Type any other character to cancel.");
			command = input.nextLine().trim();
			if (command.equals("y")) {
				query = String.format("delete from Movies where movieID = '%s';", movieID);
				st.executeUpdate(query);
				System.out.println("Movie successfully deleted.");
			} else {
				System.out.println("Action cancelled.");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void addPerson() {
		String name = "Name";
		String birthName = "Birth Name";
		String height = "Height";
		String biography = "Biography";
		String birthDetails = "Birth Details";
		String dateOfBirth = "Date of Birth";
		String dateOfDeath = "Date of Death";
		String placeOfDeath = "Place of Death";
		String reasonOfDeath = "Reason of Death";

		HashMap<String, String> values = new HashMap<String, String>();

		String command = "";
		boolean exitLoop = false;
		while (!exitLoop) {
			System.out.println("Use the following form to add a person entry.");
			System.out.println("Type the number at the beginning of the line to edit the property.");
			System.out.println("Required fields are marked with a '*'.");
			System.out.println("Type \'c\' to confirm your changes. Type \'x\' to cancel.");
			System.out.println("1: Name *");
			System.out.println("2: Birth Name");
			System.out.println("3: Height");
			System.out.println("4: Biography");
			System.out.println("5: Birth Details");
			System.out.println("6: Date of Birth");
			System.out.println("7: Date of Death");
			System.out.println("8: Place of Death");
			System.out.println("9: Reason of Death");

			command = input.nextLine();

			switch(command) {
				case "1":
					System.out.println("Enter a value for Name");
					command = input.nextLine();
					if (command.trim().equals("")) {
						System.out.println("This field is required.");
					}
					else {
						values.put(name, command);
					}
					break;
				case "2":
					System.out.println("Enter a value for Birth Name");
					command = input.nextLine();
					values.put(birthName, command);
					break;
				case "3":
					System.out.println("Enter a value for Height");
					command = input.nextLine();
					values.put(height, command);
					break;
				case "4":
					System.out.println("Enter a value for Biography");
					command = input.nextLine();
					values.put(biography, command);
					break;
				case "5":
					System.out.println("Enter a value for Birth Details");
					command = input.nextLine();
					values.put(birthDetails, command);
					break;
				case "6":
					System.out.println("Enter a value for Date of Birth (yyyy/mm/dd)");
					command = input.nextLine();
					values.put(dateOfBirth, command);
					break;
				case "7":
					System.out.println("Enter a value for Date of Death (yyyy/mm/dd)");
					command = input.nextLine();
					values.put(dateOfDeath, command);
					break;
				case "8":
					System.out.println("Enter a value for Place of Death");
					command = input.nextLine();
					values.put(placeOfDeath, command);
					break;
				case "9":
					System.out.println("Enter a value for Reason of Death");
					command = input.nextLine();
					values.put(reasonOfDeath, command);
					break;
				case "c":
					exitLoop = true;
					try {
						PreparedStatement query = c.prepareStatement("select * from MaxIDs where entityName = 'person'");
						ResultSet idrs = query.executeQuery();
						int maxId = 0;
						while (idrs.next()) {
							maxId = Integer.parseInt(idrs.getString(2));
						}
						maxId++;

						query = c.prepareStatement("update MaxIDs set entityNum = ? where entityName = 'person'");
						query.setInt(1, maxId);
						query.executeUpdate();

						query = c.prepareStatement("insert into People values (?, ?, ?, ?, ?, ?, ?)");
						query.setString(1, "nm" + maxId);
						query.setString(2, values.containsKey(name) ? values.get(name) : "");
						query.setString(3, values.containsKey(birthName) ? values.get(birthName) : "");
						query.setInt(4, values.containsKey(height) ? Integer.parseInt(values.get(height)) : 0);
						query.setString(5, values.containsKey(biography) ? values.get(biography) : "");
						query.setString(6, values.containsKey(birthDetails) ? values.get(birthDetails) : "");
						query.setTimestamp(7, values.containsKey(dateOfBirth) ? new java.sql.Timestamp(format.parse(values.get(dateOfBirth) + " 00:00:00").getTime()) : null);
						query.execute();

						if (values.containsKey(dateOfDeath) || values.containsKey(placeOfDeath) || values.containsKey(reasonOfDeath)) {
							query = c.prepareStatement("insert into DeadPeople values (?, ?, ?, ?)");
							query.setString(1, "nm" + maxId);
							query.setTimestamp(2, values.containsKey(dateOfDeath) ? new java.sql.Timestamp(format.parse(values.get(dateOfDeath) + " 00:00:00").getTime()) : null);
							query.setString(3, values.containsKey(placeOfDeath) ? values.get(placeOfDeath) : "");
							query.setString(4, values.containsKey(reasonOfDeath) ? values.get(reasonOfDeath) : "");
							query.execute();
						}
					} catch (Exception e) {
						System.out.println(e);
					}
					break;
				case "x":
					exitLoop = true;
					break;
				default:
					System.out.println("Invalid command " + command);
					break;
			}
		}
	}

	public static void editPerson(String entity) {
		try {
			PreparedStatement query = c.prepareStatement("select * from People where name like ?");
			query.setString(1, entity + "%");
			ResultSet rs = query.executeQuery();

			if (rs.next()) {
				String personID = rs.getString(1);
				String name = rs.getString(2);
				String birthName = rs.getString(3);
				int height = rs.getInt(4);
				String biography = rs.getString(5);
				String birthDetails = rs.getString(6);
				String dateOfBirth = rs.getTimestamp(7) != null ? rs.getTimestamp(7).toString().split(" ")[0].replace('-', '/') : "";
				String deadPersonID = "";
				String dateOfDeath = "";
				String placeOfDeath = "";
				String reasonOfDeath = "";
				query = c.prepareStatement("select * from DeadPeople where personID = ?");
				query.setString(1, personID);
				ResultSet rs2 = query.executeQuery();
				if (rs2.next()) {
					deadPersonID = rs2.getString(1);
					dateOfDeath = rs2.getTimestamp(2).toString() != "" ? rs2.getTimestamp(2).toString().split(" ")[0].replace('-', '/') : "";
					placeOfDeath = rs2.getString(3);
					reasonOfDeath = rs2.getString(4);
				}

				boolean exitLoop = false;
				String command = "";
				while (!exitLoop) {
					System.out.println("Type the number at the beginning of the line to edit the property.");
					System.out.println("Required fields are marked with a '*'.");
					System.out.println("Type \'c\' to confirm your changes. Type \'x\' to cancel.");
					System.out.println("1: Name * - " + name);
					System.out.println("2: Birth Name - " + birthName);
					System.out.println("3: Height - " + height);
					System.out.println("4: Biography - " + (biography != "" ? biography.substring(0, Math.min(100, biography.length())) : "") + (biography.length() > 100 ? "..." : ""));
					System.out.println("5: Birth Details - " + birthDetails);
					System.out.println("6: Date of Birth - " + dateOfBirth);
					System.out.println("7: Date of Death - " + dateOfDeath);
					System.out.println("8: Place of Death - " + placeOfDeath);
					System.out.println("9: Reason of Death - " + reasonOfDeath);
					command = input.nextLine();

					switch(command) {
						case "1":
							System.out.println("Enter a new value for Name");
							command = input.nextLine();
							if (command.trim().equals("")) {
								System.out.println("This field is required.");
							}
							else {
								name = command;
							}
							break;
						case "2":
							System.out.println("Enter a new value for Birth Name");
							command = input.nextLine();
							birthName = command;
							break;
						case "3":
							System.out.println("Enter a new value for Height");
							command = input.nextLine();
							height = Integer.parseInt(command);
							break;
						case "4":
							System.out.println("Enter a new value for Biography");
							command = input.nextLine();
							biography = command;
							break;
						case "5":
							System.out.println("Enter a new value for Birth Details");
							command = input.nextLine();
							birthDetails = command;
							break;
						case "6":
							System.out.println("Enter a new value for Date of Birth (yyyy/mm/dd)");
							command = input.nextLine();
							dateOfBirth = command;
							break;
						case "7":
							System.out.println("Enter a new value for Date of Death (yyyy/mm/dd)");
							command = input.nextLine();
							dateOfDeath = command;
							break;
						case "8":
							System.out.println("Enter a new value for Place of Death");
							command = input.nextLine();
							placeOfDeath = command;
							break;
						case "9":
							System.out.println("Enter a new value for Reason of Death");
							command = input.nextLine();
							reasonOfDeath = command;
							break;
						case "c":
							exitLoop = true;
							if (dateOfDeath.equals("") && placeOfDeath.equals("") && reasonOfDeath.equals("")) {
								query = c.prepareStatement("delete ignore from DeadPeople where personID = ?");
								query.setString(1, personID);
								query.executeUpdate();
							}
							else {
								if (deadPersonID.equals("")) {	
									query = c.prepareStatement("insert into DeadPeople values (?, ?, ?, ?)");
									query.setString(1, personID);
									query.setTimestamp(2, dateOfDeath != "" ? new java.sql.Timestamp(format.parse(dateOfDeath + " 00:00:00").getTime()) : null);
									query.setString(3, placeOfDeath);
									query.setString(4, reasonOfDeath);
									query.executeUpdate();
								}
							}
							PreparedStatement query2 = c.prepareStatement("update People set name = ?, birthName = ?, height = ?, bio = ?, birthDetails = ?, dateOfBirth = ? where personID = ?");
							query2.setString(1, name);
							query2.setString(2, birthName);
							query2.setInt(3, height);
							query2.setString(4, biography);
							query2.setString(5, birthDetails);
							query2.setTimestamp(6, dateOfBirth != "" ? new java.sql.Timestamp(format.parse(dateOfBirth + " 00:00:00").getTime()) : null);
							query2.setString(7, personID);
							query2.executeUpdate();
							System.out.println("Successfully updated person with personID = " + personID);
							System.out.println();
							break;
						case "x":
							exitLoop = true;
							break;
						default:
							System.out.println("Invalid command " + command);
							break;
					}
				}
			}
			else {
				System.out.println("A person with this name does not exist.");
			}
		} catch (Exception e) {
			System.out.println(e);
		}
	}

	public static void deletePerson(String q) {
		try {
			PreparedStatement query = c.prepareStatement("select personID, name from People where name like ? limit 10");
			query.setString(1, q + "%");
			ResultSet rs = query.executeQuery();

			String people[] = new String[10];
			System.out.println("Select a person to delete");
			int i = 1;
			while (rs.next()) {
				people[i - 1] = rs.getString(1);
				System.out.println(i + ": " + rs.getString(2));
				i++;
			}
			String command = input.nextLine().trim();
			String personID = people[Integer.parseInt(command) - 1];
			System.out.println("Are you sure you want to delete this person? Type y to confirm. Type any other character to cancel.");
			command = input.nextLine().trim();
			if (command.equals("y")) {
				query = c.prepareStatement("delete ignore from DeadPeople where personID = ?");
				query.setString(1, personID);
				query.executeUpdate();

				query = c.prepareStatement("delete ignore from PeopleInMovies where personID = ?");
				query.setString(1, personID);
				query.executeUpdate();

				query = c.prepareStatement("delete from People where personID = ?");
				query.setString(1, personID);
				query.executeUpdate();

				System.out.println("Person successfully deleted.");
			} else {
				System.out.println("Action cancelled.");
			}
		} catch (Exception e) {
			System.out.println(e);
		}

	}
}
