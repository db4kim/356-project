public class Command {
    private String name;
    private String cmd;
    private String usage;
    private String description;

    public Command(String name, String cmd, String usage, String description) {
        this.name = name;
        this.cmd = cmd;
        this.usage = usage;
        this.description = description;
    }

    public String getName() {
        return this.name;
    }

    public String getCmd() {
        return this.cmd;
    }
    
    public String getUsage() {
        return this.usage;
    }

    public String getDescription() {
        return this.description;
    }
}
