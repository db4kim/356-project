JFLAGS = -g
JC = javac
J = java
.SUFFIXES: .java .class
.java.class:
	$(JC) $(JFLAGS) $*.java

CLASSES = \
	  ClientApplication.java \
	  Command.java \

default: classes

classes: $(CLASSES:.java=.class)

run:
	$(J) -cp .:mysql-connector-java-8.0.27.jar ClientApplication

clean:
	$(RM) *.class
