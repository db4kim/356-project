drop table if exists MaxIDs;
drop table if exists ProductionCompaniesInMovies;
drop table if exists ProductionCompany;
drop table if exists LanguagesInMovies;
drop table if exists Languages;
drop table if exists CountriesInMovies;
drop table if exists Countries;
drop table if exists GenresInMovies;
drop table if exists Genres;
drop table if exists Reviews;
drop table if exists UsersFavouriteMovies;
drop table if exists Users;
drop table if exists PeopleInMovies;
drop table if exists DeadPeople;
drop table if exists People;
drop table if exists Budget;
drop table if exists Income;
drop table if exists Movies;

create table Movies (
  movieID char(9) not null,
  title varchar(256),
  originalTitle varchar(256),
  year int,
  releaseDate datetime,
  duration int,
  description text,
  averageRating float,
  totalRating int,
  metascore float
);
alter table Movies add primary key (movieID);
replace into Movies (movieID, title) select movieID, title from MojoBudgetData;
replace into Movies (movieID, title, releaseDate, description) select movieID, title, releaseDate, tagline from MoviesMetadata;
replace into Movies (movieID, title, releaseDate, description) select movieID, title, releaseDate, trivia from MojoBudgetUpdate;
replace into Movies (movieID, title, originalTitle, year, releaseDate, duration, description, averageRating, totalRating, metascore)
select movieID, title, originalTitle, year, releaseDate, duration, description, score, votes, metascore from ImdbMovies;
alter table Movies add index yrIndex(year);

create table Income (
  movieID char(9) not null,
  incomeDomestic bigint,
  incomeWorldwide bigint
);
alter table Income add primary key (movieID);
alter table Income add foreign key (movieID) references Movies(movieID);
replace into Income (movieID, incomeWorldwide) select movieID, revenue from MoviesMetadata;
replace into Income (movieID, incomeDomestic, incomeWorldwide) select movieID, domestic, worldwide from MojoBudgetData;
replace into Income (movieID, incomeDomestic, incomeWorldwide) select movieID, domestic, worldwide from MojoBudgetUpdate;
replace into Income (movieID, incomeDomestic, incomeWorldwide) select
  movieID,
  cast(regexp_substr(grossIncomeUSA, '[^$ ]*$') as unsigned),
  cast(regexp_substr(grossIncomeWorldwide, '[^$ ]*$') as unsigned)
from ImdbMovies;

create table Budget (
  movieID char(9) not null,
  budget bigint
);
alter table Budget add primary key (movieID);
alter table Budget add foreign key (movieID) references Movies(movieID);
replace into Budget (movieID, budget) select movieID, budget from MojoBudgetData;
replace into Budget (movieID, budget) select movieID, budget from MojoBudgetUpdate;
replace into Budget (movieID, budget) select
  movieID,
  cast(regexp_substr(budget, '[^$ ]*$') as unsigned)
from ImdbMovies;

-- person stuff
create table People (
  personID char(9) not null,
  name varchar(64),
  birthName varchar(256),
  height int,
  bio text,
  birthDetails varchar(256),
  dateOfBirth datetime
);
alter table People add primary key (personID);
replace into People (personID, name, birthName, height, bio, birthDetails, dateOfBirth)
select personID, name, birthName, height, bio, birthDetails, dateOfBirth from ImdbNames;

create table DeadPeople (
  personID char(9) not null,
  dateOfDeath datetime,
  placeOfDeath varchar(128),
  reasonOfDeath varchar(128)
);
alter table DeadPeople add primary key (personID);
alter table DeadPeople add foreign key (personID) references People(personID);
replace into DeadPeople (personID, dateOfDeath, placeOfDeath, reasonOfDeath)
select personID, dateOfDeath, placeOfDeath, reasonOfDeath from ImdbNames where dateOfDeath is not null;

create table PeopleInMovies (
  movieID char(9) not null,
  personID char(9) not null,
  role varchar(32)
);
alter table PeopleInMovies add primary key (movieID, personID);
alter table PeopleInMovies add foreign key (movieID) references Movies(movieID);
alter table PeopleInMovies add foreign key (personID) references People(personID);
replace into PeopleInMovies (movieID, personID, role) select movieID, personID, category from ImdbTitlePrincipals;

-- user stuff
create table Users (
  userID varchar(32) not null
);
alter table Users add primary key (userID);

create table UsersFavouriteMovies (
  userID varchar(32) not null,
  movieID char(9) not null
);
alter table UsersFavouriteMovies add primary key (userID, movieID);
alter table UsersFavouriteMovies add foreign key (userID) references Users(userID);
alter table UsersFavouriteMovies add foreign key (movieID) references Movies(movieID);

create table Reviews (
  movieID char(9) not null,
  userID varchar(32) not null,
  comment text
);
alter table Reviews add primary key (movieID, userID);
alter table Reviews add foreign key (movieID) references Movies(movieID);
alter table Reviews add foreign key (userID) references Users(userID);

-- genre stuff
create table Genres (
  genre varchar(16) not null
);
alter table Genres add primary key (genre);

drop table if exists temp;
create temporary table temp (val varchar(32));
drop procedure if exists parseGenre;
delimiter ;;
create procedure parseGenre() begin
  declare n int default 0;
  declare i int default 0;
  select count(distinct genre) from ImdbMovies into n;
  insert into temp (val) select distinct genre from ImdbMovies;
  set i = 0;
  while i < n do
    set @s = concat(
      "replace into Genres (genre) values ('",
      replace(
        (select * from temp limit i, 1),
        ", ",
        "'),('"
      ),
      "');"
    );
    prepare stmt from @s;
    execute stmt;
    set i = i + 1;
  end while;
end;
;;
delimiter ;
call parseGenre();
drop table if exists temp;

create table GenresInMovies (
  movieID char(9) not null,
  genre varchar(16) not null
);
alter table GenresInMovies add primary key (movieID, genre);
alter table GenresInMovies add foreign key (movieID) references Movies(movieID);
alter table GenresInMovies add foreign key (genre) references Genres(genre);

drop procedure if exists loadGenre;
delimiter ;;
create procedure loadGenre() begin
  declare n int default 0;
  declare i int default 0;
  select count(*) from ImdbMovies into n;
  set i = 0;
  while i < n do
    set @s = concat(
      "insert into GenresInMovies (movieID, genre) values ('",
      (select movieID from ImdbMovies limit i, 1),
      "','",
      replace(
        (select genre from ImdbMovies limit i, 1),
        ", ",
        concat("'),('", (select movieID from ImdbMovies limit i, 1), "','")
      ),
      "');"
    );
    prepare stmt from @s;
    execute stmt;
    set i = i + 1;
  end while;
end;
;;
delimiter ;
call loadGenre();

-- country stuff
create table Countries (
  country varchar(128) not null
);
alter table Countries add primary key (country);
replace into Countries (country) select distinct country from ImdbMovies where country != "";

-- drop table if exists temp;
-- create table temp (val varchar(128));
-- drop procedure if exists parseCountry;
-- delimiter ;;
-- create procedure parseCountry() begin
--   declare n int default 0;
--   declare i int default 0;
--   select count(distinct country) from ImdbMovies into n;
--   insert into temp (val) select distinct country from ImdbMovies;
--   set i = 0;
--   while i < n do
--     set @s = concat(
--       "replace into Countries (country) values ('",
--       replace(
--         replace(
--           replace(
--             (select val from temp limit i, 1),
--             "'",
--             ""
--           ),
--           ",",
--           "'),('"
--         ),
--         " ",
--         ""
--       ),
--       "');"
--     );
--     prepare stmt from @s;
--     execute stmt;
--     set i = i + 1;
--   end while;
-- end;
-- ;;
-- delimiter ;
-- call parseCountry();
-- drop table if exists temp;
-- alter table Countries add index countryIndex(country);

create table CountriesInMovies (
  movieID char(9) not null,
  country varchar(128) not null
);
alter table CountriesInMovies add primary key (movieID, country);
alter table CountriesInMovies add foreign key (movieID) references Movies(movieID);
alter table CountriesInMovies add foreign key (country) references Countries(country);
replace into CountriesInMovies (movieID, country) select movieID, country from ImdbMovies where country != "";

-- drop procedure if exists loadCountry;
-- delimiter ;;
-- create procedure loadCountry() begin
--   declare n int default 0;
--   declare i int default 0;
--   select count(*) from ImdbMovies into n;
--   set i = 0;
--   while i < n do
--     set @s = concat(
--       "replace into CountriesInMovies (movieID, country) values ('",
--       (select movieID from ImdbMovies limit i, 1),
--       "','",
--       replace(
--         replace(
--           replace(
--             (select country from ImdbMovies limit i, 1),
--             "'",
--             ""
--           ),
--           ",",
--           concat("'),('", (select movieID from ImdbMovies limit i, 1), "','")
--         ),
--         " ",
--         ""
--       ),
--       "');"
--     );
--     prepare stmt from @s;
--     execute stmt;
--     set i = i + 1;
--   end while;
-- end;
-- ;;
-- delimiter ;
-- call loadCountry();

-- language stuff
create table Languages (
  language varchar(128) not null
);
alter table Languages add primary key (language);
replace into Languages (language) select distinct language from ImdbMovies where language != "";

-- drop table if exists temp;
-- create table temp (val varchar(128));
-- drop procedure if exists parseLanguage;
-- delimiter ;;
-- create procedure parseLanguage() begin
--   declare n int default 0;
--   declare i int default 0;
--   select count(distinct language) from ImdbMovies into n;
--   insert into temp (val) select distinct language from ImdbMovies;
--   set i = 0;
--   while i < n do
--     set @s = concat(
--       "replace into Languages (language) values ('",
--       replace(
--         replace(
--           replace(
--             (select * from temp limit i, 1),
--             "'",
--             ""
--           ),
--           ", ",
--           "'),('"
--         ),
--         " ",
--         ""
--       ),
--       "');"
--     );
--     prepare stmt from @s;
--     execute stmt;
--     set i = i + 1;
--   end while;
-- end;
-- ;;
-- delimiter ;
-- call parseLanguage();
-- alter table Languages add index languageIndex(language);

create table LanguagesInMovies (
  movieID char(9) not null,
  language varchar(128) not null
);
alter table LanguagesInMovies add primary key (movieID, language);
alter table LanguagesInMovies add foreign key (movieID) references Movies(movieID);
alter table LanguagesInMovies add foreign key (language) references Languages(language);
replace into LanguagesInMovies (movieID, language) select movieID, language from ImdbMovies where language != "";
-- drop procedure if exists loadLanguage;
-- delimiter ;;
-- create procedure loadLanguage() begin
--   declare n int default 0;
--   declare i int default 0;
--   select count(*) from ImdbMovies into n;
--   set i = 0;
--   while i < 5000 do
--     set @s = concat(
--       "replace into LanguagesInMovies (movieID, language) values ('",
--       (select movieID from ImdbMovies limit i, 1),
--       "','",
--       replace(
--         replace(
--           replace(
--             (select language from ImdbMovies limit i, 1),
--             "'",
--             ""
--           ),
--           ", ",
--           concat("'),('", (select movieID from ImdbMovies limit i, 1), "','")
--         ),
--         " ",
--         ""
--       ),
--       "');"
--     );
--     prepare stmt from @s;
--     execute stmt;
--     set i = i + 1;
--   end while;
-- end;
-- ;;
-- delimiter ;
-- call loadLanguage();

-- production company stuff
create table ProductionCompany (
  productionCompany varchar(128) not null
);
alter table ProductionCompany add primary key (productionCompany);
replace into ProductionCompany (productionCompany) select distinct productionCompany from ImdbMovies where productionCompany != "";
alter table ProductionCompany add index productionCompanyIndex(productionCompany);

create table ProductionCompaniesInMovies (
  movieID char(9) not null,
  productionCompany varchar(128) not null
);
alter table ProductionCompaniesInMovies add primary key (movieID, productionCompany);
alter table ProductionCompaniesInMovies add foreign key (movieID) references Movies(movieID);
alter table ProductionCompaniesInMovies add foreign key (productionCompany) references ProductionCompany(productionCompany);
replace into ProductionCompaniesInMovies (movieID, productionCompany) select movieID, productionCompany from ImdbMovies where productionCompany != "";

-- max id
create table MaxIDs (
  entityName varchar(8),
  entityNum int
);
alter table MaxIDs add primary key (entityName);
replace into MaxIDs (entityName, entityNum) values (
  "movie",
  cast(regexp_substr((select movieID from Movies order by movieID desc limit 1), '[^tt]*$') as unsigned)
);
replace into MaxIDs (entityName, entityNum) values (
  "person",
  cast(regexp_substr((select personID from People order by personID desc limit 1), '[^nm]*$') as unsigned)
);
